import erpnext
import frappe
from erpnext.accounts.doctype.gl_entry.gl_entry import GLEntry
from erpnext.projects.doctype.task.task import Task
from erpnext.projects.doctype.timesheet.timesheet import Timesheet
from frappe import _
from frappe.utils import date_diff, flt, time_diff_in_hours
from frappe.utils.nestedset import get_ancestors_of
from hrms.payroll.doctype.payroll_entry.payroll_entry import PayrollEntry


class CustomGLEntry(GLEntry):
    def validate_cost_center(self):
        if not self.cost_center:
            return

        is_group = frappe.db.get_value(
            "Cost Center",
            self.cost_center,
            "is_group",
        )

        if self.voucher_type != "Period Closing Voucher" and is_group:
            frappe.throw(
                _(
                    """{0} {1}: Cost Center {2} is a group cost center and group cost centers cannot be used in transactions"""  # noqa: E501
                ).format(
                    self.voucher_type,
                    self.voucher_no,
                    frappe.bold(self.cost_center),
                )
            )


class CustomPayrollEntry(PayrollEntry):
    def make_accrual_jv_entry(self):
        earnings = (
            self.get_salary_component_total(component_type="earnings") or {}
        )  # noqa: E501
        deductions = (
            self.get_salary_component_total(component_type="deductions") or {}
        )  # noqa: E501
        payroll_payable_account = self.payroll_payable_account
        jv_name = ""
        precision = frappe.get_precision(
            "Journal Entry Account", "debit_in_account_currency"
        )

        if earnings or deductions:
            journal_entry = frappe.new_doc("Journal Entry")
            journal_entry.voucher_type = "Journal Entry"
            journal_entry.user_remark = _(
                "Accrual Journal Entry for salaries from {0} to {1}"
            ).format(self.start_date, self.end_date)
            journal_entry.company = self.company
            journal_entry.posting_date = self.posting_date

            accounts = []
            currencies = []
            payable_amount = 0
            multi_currency = 0
            company_currency = erpnext.get_company_currency(self.company)

            # Earnings
            for acc_cc, amount in earnings.items():
                (
                    exchange_rate,
                    amt,
                ) = self.get_amount_and_exchange_rate_for_journal_entry(
                    acc_cc[0], amount, company_currency, currencies
                )
                payable_amount += flt(amount, precision)
                accounts.append(
                    {
                        "account": acc_cc[0],
                        "debit_in_account_currency": flt(amt, precision),
                        "exchange_rate": flt(exchange_rate),
                        "party_type": "",
                        "cost_center": acc_cc[1] or self.cost_center,
                        "project": self.project,
                    }
                )

            # Deductions
            for acc_cc, amount in deductions.items():
                (
                    exchange_rate,
                    amt,
                ) = self.get_amount_and_exchange_rate_for_journal_entry(
                    acc_cc[0], amount, company_currency, currencies
                )
                payable_amount -= flt(amount, precision)
                accounts.append(
                    {
                        "account": acc_cc[0],
                        "credit_in_account_currency": flt(amt, precision),
                        "exchange_rate": flt(exchange_rate),
                        "cost_center": acc_cc[1] or self.cost_center,
                        "party_type": "",
                        "project": self.project,
                    }
                )

            # Payable amount
            (
                exchange_rate,
                payable_amt,
            ) = self.get_amount_and_exchange_rate_for_journal_entry(
                payroll_payable_account,
                payable_amount,
                company_currency,
                currencies,
            )
            accounts.append(
                {
                    "account": payroll_payable_account,
                    "credit_in_account_currency": flt(payable_amt, precision),
                    "exchange_rate": flt(exchange_rate),
                    "party_type": "",
                    "cost_center": self.cost_center,
                }
            )

            journal_entry.set("accounts", accounts)
            if len(currencies) > 1:
                multi_currency = 1
            journal_entry.multi_currency = multi_currency
            journal_entry.title = payroll_payable_account
            journal_entry.save()

            try:
                journal_entry.submit()
                jv_name = journal_entry.name
                self.update_salary_slip_status(jv_name=jv_name)
            except Exception as e:
                if type(e) in (str, list, tuple):
                    frappe.msgprint(e)
                raise

        return jv_name


class CustomTask(Task):
    def validate(self):
        super().validate()
        self.validate_task_perm()

    def validate_task_perm(self):
        if "Accounts Manager" in frappe.get_roles(
            frappe.session.user
        ) or "System Manager" in frappe.get_roles(frappe.session.user):
            pass
        else:
            if (
                not self.parent_task
                and self.is_group == 0
                and ("Freelancer" in frappe.get_roles(frappe.session.user))
            ):
                frappe.throw(
                    _(
                        "Please enter group task as it is mandatory for your roles to create a task."  # noqa: E501
                    )
                )
            if self.parent_task and (
                "Freelancer" in frappe.get_roles(frappe.session.user)
            ):
                group_permissions = frappe.db.sql(
                    """
                          select
                            name
                          from
                            `tabUser Permission`
                          where
                            user = %s and allow = 'Task' and for_value = %s
                          """,
                    (frappe.session.user, self.parent_task),
                )
                if group_permissions:
                    pass
                else:
                    frappe.throw(
                        _(
                            f"You do not have permissions to create tasks under Parent Task {self.parent_task}"  # noqa: E501
                        )
                    )


class CustomTimesheet(Timesheet):
    def validate(self):
        self.set_status()
        self.validate_dates()
        self.validate_time_logs()
        self.calculate_std_hours()
        self.update_cost()
        self.calculate_total_amounts()
        self.calculate_percentage_billed()
        self.set_dates()

    def update_billing_hours(self, args):
        if args.is_billable:
            args.billing_hours = args.hours
        else:
            args.billing_hours = args.hours

    def calculate_std_hours(self):
        std_working_hours = frappe.get_value(
            "Company", self.company, "standard_working_hours"
        )
        for time in self.time_logs:
            if time.from_time and time.to_time:
                if flt(std_working_hours) and date_diff(
                    time.to_time, time.from_time
                ):  # noqa: E501
                    time.hours = flt(std_working_hours) * date_diff(
                        time.to_time, time.from_time
                    )
                else:
                    if not time.hours:
                        time.hours = time_diff_in_hours(
                            time.to_time, time.from_time
                        )  # noqa: E501


def projects_hourly_reminder():
    def get_project_totals(project):
        frappe.db.sql(
            """
            update `tabTask`
              set total_billing_amount = 0,
              total_costing_amount = 0,
              last_run_on = addtime(now(),'05:30:00.000000')
            where
              project = %s and is_group = 1
            """,
            (project),
        )

        get_gt = frappe.db.sql(
            """
            select
              name,
              sum(total_billing_amount) as bill_amt,
              sum(total_costing_amount) as cost_amt
            from
              `tabTask`
            where
              project = %s and is_group = 0
            group by
              name
            """,
            (project),
            as_dict=1,
        )

        for gt in get_gt:
            parents = get_ancestors_of(
                "Task", gt["name"], order_by="lft desc", limit=None
            )
            for index in range(len(parents)):
                frappe.db.sql(
                    """
                  update `tabTask`
                  set total_billing_amount = total_billing_amount + %s,
                  total_costing_amount = total_costing_amount + %s,
                  last_run_on = addtime(now(),'05:30:00.000000')
                  where project = %s and name = %s
                  """,
                    (gt["bill_amt"], gt["cost_amt"], project, parents[index]),
                )
        frappe.db.commit()

    projects = frappe.db.sql(
        """
        select
          name
        from
          `tabProject`
        """,
        as_dict=1,
    )
    for project in projects:
        get_project_totals(project["name"])


def project_daily_reminder():
    def get_project_totals(project):
        frappe.db.sql(
            """
            update
              `tabTask`
            set
              total_billing_amount = 0,
              total_costing_amount = 0,
              last_run_on = addtime(now(),'05:30:00.000000')
            where
              project = %s and is_group = 1
            """,
            (project),
        )

        get_gt = frappe.db.sql(
            """
                select
                  name,
                  sum(total_billing_amount) as bill_amt,
                  sum(total_costing_amount) as cost_amt
                from
                  `tabTask`
                where
                  project = %s and is_group = 0
                group by
                  name
                """,
            (project),
            as_dict=1,
        )

        for gt in get_gt:
            parents = get_ancestors_of(
                "Task", gt["name"], order_by="lft desc", limit=None
            )
            for index in range(len(parents)):
                frappe.db.sql(
                    """
                  update `tabTask`
                  set total_billing_amount = total_billing_amount + %s,
                  total_costing_amount = total_costing_amount + %s,
                  last_run_on = addtime(now(),'05:30:00.000000')
                  where project = %s and name = %s
                  """,
                    (gt["bill_amt"], gt["cost_amt"], project, parents[index]),
                )
        frappe.db.commit()

    projects = frappe.db.sql(
        """
                  select
                    name
                  from
                    `tabProject`
                """,
        as_dict=1,
    )
    for project in projects:
        get_project_totals(project["name"])

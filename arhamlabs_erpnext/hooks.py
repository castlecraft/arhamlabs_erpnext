from . import __version__

app_name = "arhamlabs_erpnext"
app_title = "ArhamLabs ERPNext"
app_publisher = "Castlecraft Ecommerce Pvt. Ltd."
app_description = "ERPNext Customization for Arham Labs"
app_email = "support@castlecraft.in"
app_license = "MIT"
app_version = __version__

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/arhamlabs_erpnext/css/arhamlabs_erpnext.css"
# app_include_js = "/assets/arhamlabs_erpnext/js/arhamlabs_erpnext.js"

# include js, css files in header of web template
# web_include_css = "/assets/arhamlabs_erpnext/css/arhamlabs_erpnext.css"
# web_include_js = "/assets/arhamlabs_erpnext/js/arhamlabs_erpnext.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "arhamlabs_erpnext/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
# 	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Jinja
# ----------

# add methods and filters to jinja environment
# jinja = {
# 	"methods": "arhamlabs_erpnext.utils.jinja_methods",
# 	"filters": "arhamlabs_erpnext.utils.jinja_filters"
# }

# Installation
# ------------

# before_install = "arhamlabs_erpnext.install.before_install"
# after_install = "arhamlabs_erpnext.install.after_install"

# Uninstallation
# ------------

# before_uninstall = "arhamlabs_erpnext.uninstall.before_uninstall"
# after_uninstall = "arhamlabs_erpnext.uninstall.after_uninstall"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "arhamlabs_erpnext.notifications.get_notification_config"  # noqa: E501

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

override_doctype_class = {
    "GLEntry": "arhamlabs_erpnext.overrides.CustomGLEntry",
    # "PayrollEntry": "arhamlabs_erpnext.overrides.CustomPayrollEntry",
    "Task": "arhamlabs_erpnext.overrides.CustomTask",
    "Timesheet": "arhamlabs_erpnext.overrides.CustomTimesheet",
}

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
# 	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"arhamlabs_erpnext.tasks.all"
# 	],
# 	"daily": [
# 		"arhamlabs_erpnext.tasks.daily"
# 	],
# 	"hourly": [
# 		"arhamlabs_erpnext.tasks.hourly"
# 	],
# 	"weekly": [
# 		"arhamlabs_erpnext.tasks.weekly"
# 	],
# 	"monthly": [
# 		"arhamlabs_erpnext.tasks.monthly"
# 	],
# }

# Testing
# -------

# before_tests = "arhamlabs_erpnext.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "arhamlabs_erpnext.event.get_events"  # noqa: E501
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "arhamlabs_erpnext.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]

# Ignore links to specified DocTypes when deleting documents
# -----------------------------------------------------------

# ignore_links_on_delete = ["Communication", "ToDo"]

# Request Events
# ----------------
# before_request = ["arhamlabs_erpnext.utils.before_request"]
# after_request = ["arhamlabs_erpnext.utils.after_request"]

# Job Events
# ----------
# before_job = ["arhamlabs_erpnext.utils.before_job"]
# after_job = ["arhamlabs_erpnext.utils.after_job"]

# User Data Protection
# --------------------

# user_data_fields = [
# 	{
# 		"doctype": "{doctype_1}",
# 		"filter_by": "{filter_by}",
# 		"redact_fields": ["{field_1}", "{field_2}"],
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_2}",
# 		"filter_by": "{filter_by}",
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_3}",
# 		"strict": False,
# 	},
# 	{
# 		"doctype": "{doctype_4}"
# 	}
# ]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"arhamlabs_erpnext.auth.validate"
# ]

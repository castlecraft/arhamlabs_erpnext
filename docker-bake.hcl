variable "PYTHON_VERSION" {
  default = "3.11.4"
}

variable "NODE_VERSION" {
  default = "16.20.1"
}

variable "FRAPPE_PATH" {
  default = "https://github.com/frappe/frappe"
}

variable "FRAPPE_BRANCH" {
  default = "v14.40.3"
}

variable "APPS_JSON_BASE64" {}

variable "REGISTRY_NAME" {
  default = "registry.gitlab.com/castlecraft/arhamlabs_erpnext"
}

variable "IMAGE_NAME" {
  default = "bench"
}

variable "VERSION" {
  default = "latest"
}

target "default" {
    dockerfile = "image/Containerfile"
    tags = ["${REGISTRY_NAME}/${IMAGE_NAME}:${VERSION}"]
    args = {
      "PYTHON_VERSION" = PYTHON_VERSION
      "NODE_VERSION" = NODE_VERSION
      "FRAPPE_PATH" = FRAPPE_PATH
      "FRAPPE_BRANCH" = FRAPPE_BRANCH
      "APPS_JSON_BASE64" = APPS_JSON_BASE64
    }
}

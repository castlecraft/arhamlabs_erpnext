from frappe import _


def get_data():
    return [
        {
            "module_name": "ArhamLabs ERPNext",
            "type": "module",
            "label": _("ArhamLabs ERPNext"),
        }
    ]

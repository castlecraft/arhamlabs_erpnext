import os

from setuptools import find_packages, setup

from arhamlabs_erpnext import __version__ as version
from arhamlabs_erpnext.hooks import app_description, app_email, app_publisher

requirements_file = os.environ.get("REQUIREMENTS_FILE", "requirements.txt")

with open(requirements_file) as f:
    install_requires = f.read().strip().split("\n")


setup(
    name="arhamlabs_erpnext",
    version=version,
    description=app_description,
    author=app_publisher,
    author_email=app_email,
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
)
